defmodule Manganese.SerializationKitTest.Structs.UnityTransformComponentTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityTransformComponent

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityTransformComponent.from_map/1`" do
    @tag unity_component: true, unity_transform_component: true
    test "it deserializes a Unity transform component from a map" do
      map = %{
        "id" => 1337,
        "type" => Enumerations.UnityComponentType.to_integer(:transform)
      }
      transform = Structs.UnityTransformComponent.from_map map

      assert transform.id == map["id"]
      assert transform.type == Enumerations.UnityComponentType.from_integer(map["type"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityTransformComponent.to_map/1`" do
    @tag unity_component: true, unity_transform_component: true
    test "it serializes a Unity transform component to a map" do
      transform = %Structs.UnityTransformComponent{
        id: 1337
      }
      map = Structs.UnityTransformComponent.to_map transform

      assert map["id"] == transform.id
      assert map["type"] == Enumerations.UnityComponentType.to_integer(transform.type)
    end
  end
end
