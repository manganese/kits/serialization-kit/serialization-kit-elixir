defmodule Manganese.SerializationKitTest.Structs.UnityComponentTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityComponent

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityComponent.from_map/1`" do
    @tag unity_component: true
    test "it deserializes a Unity component of an unknown type from a map with the generic deserializer" do
      map = %{
        "id" => 1337,
        "type" => Enumerations.UnityComponentType.to_integer(:material)
      }
      component = Structs.UnityComponent.from_map map

      assert component.id == map["id"]
      assert component.type == Enumerations.UnityComponentType.from_integer(map["type"])
    end

    @tag unity_component: true, unity_transform_component: true
    test "it deserializes a Unity transform component from a map with the generic deserializer" do
      map = %{
        "id" => 1337,
        "type" => Enumerations.UnityComponentType.to_integer(:transform)
      }
      transform = Structs.UnityComponent.from_map map

      assert %Structs.UnityTransformComponent{} = transform
    end

    @tag unity_component: true, unity_box_collider_component: true
    test "it deserializes a Unity box collider component from a map with the generic deserializer" do
      map = %{
        "id" => 1337,
        "type" => Enumerations.UnityComponentType.to_integer(:box_collider)
      }
      box_collider = Structs.UnityComponent.from_map map

      assert %Structs.UnityBoxColliderComponent{} = box_collider
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityComponent.to_map/1`" do
    @tag unity_component: true
    test "it serializes a Unity component of an unknown type to a map with the generic serializer" do
      component = %Structs.UnityComponent{
        id: 1337
      }
      map = Structs.UnityComponent.to_map component
      
      assert map["id"] == component.id
    end

    @tag unity_component: true, unity_transform_component: true
    test "it serializes a Unity transform component to a map with the generic serializer" do
      transform = %Structs.UnityTransformComponent{
        id: 1337
      }
      map = Structs.UnityComponent.to_map transform

      assert map["id"] == transform.id
      assert map["type"] == Enumerations.UnityComponentType.to_integer(transform.type)
    end

    @tag unity_component: true, unity_box_collider_component: true
    test "it serializes a Unity box collider component to a map with the generic serializer" do
      box_collider = %Structs.UnityBoxColliderComponent{
        id: 1337
      }
      map = Structs.UnityComponent.to_map box_collider

      assert map["id"] == box_collider.id
      assert map["type"] == Enumerations.UnityComponentType.to_integer(box_collider.type)
    end
  end
end
