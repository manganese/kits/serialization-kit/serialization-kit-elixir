defmodule Manganese.SerializationKitTest.Structs.UnityBoxColliderComponentTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityBoxColliderComponent

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityBoxColliderComponent.from_map/1`" do
    @tag unity_component: true, unity_box_collider_component: true
    test "it deserializes a Unity box collider component from a map" do
      map = %{
        "id" => 1337,
        "type" => Enumerations.UnityComponentType.to_integer(:box_collider)
      }
      box_collider = Structs.UnityBoxColliderComponent.from_map map

      assert box_collider.id == map["id"]
      assert box_collider.type == Enumerations.UnityComponentType.from_integer(map["type"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityBoxColliderComponent.to_map/1`" do
    @tag unity_component: true, unity_box_collider_component: true
    test "it serializes a Unity box collider component to a map" do
      box_collider = %Structs.UnityBoxColliderComponent{
        id: 1337
      }
      map = Structs.UnityBoxColliderComponent.to_map box_collider

      assert map["id"] == box_collider.id
      assert map["type"] == Enumerations.UnityComponentType.to_integer(box_collider.type)
    end
  end
end
