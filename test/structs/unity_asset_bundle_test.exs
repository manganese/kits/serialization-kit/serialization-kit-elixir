defmodule Manganese.SerializationKitTest.Structs.UnityAssetBundleTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityAssetBundle

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityAssetBundle.from_map/1`" do
    @tag unity_asset_bundle: true
    test "it deserializes a Unity asset bundle name from a map" do
      map = %{
        "name" => "Tree",
        "assets" => []
      }
      asset_bundle = Structs.UnityAssetBundle.from_map map

      assert asset_bundle.name == map["name"]
    end

    @tag unity_asset_bundle: true, unity_asset: true
    test "it deserializes a Unity asset bundle asset from a map" do
      map = %{
        "name" => "Tree",
        "assets" => [
          %{
            "id" => 1337,
            "type" => 8,
            "path" => "assets/tree.prefab",
            "name" => "Tree"
          }
        ]
      }
      asset_bundle = Structs.UnityAssetBundle.from_map map

      assert Enum.at(asset_bundle.assets, 0).id == Enum.at(map["assets"], 0)["id"]
      assert Enum.at(asset_bundle.assets, 0).type == Enumerations.UnityAssetType.from_integer(Enum.at(map["assets"], 0)["type"])
      assert Enum.at(asset_bundle.assets, 0).path == Enum.at(map["assets"], 0)["path"]
      assert Enum.at(asset_bundle.assets, 0).name == Enum.at(map["assets"], 0)["name"]
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityAssetBundle.to_map/1`" do
    @tag unity_asset_bundle: true
    test "it serializes a Unity asset bundle name to a map" do
      asset_bundle = %Structs.UnityAssetBundle{
        name: "Tree",
        assets: []
      }
      map = Structs.UnityAssetBundle.to_map asset_bundle

      assert map["name"] == asset_bundle.name
    end

    @tag unity_asset_bundle: true, unity_asset: true
    test "it serializes a Unity asset bundle asset to a map" do
      asset_bundle = %Structs.UnityAssetBundle{
        name: "Tree",
        assets: [
          %Structs.UnityAsset{
            id: 1337,
            type: 8,
            path: "assets/tree.prefab",
            name: "Tree"
          }
        ]
      }
      map = Structs.UnityAssetBundle.to_map asset_bundle

      assert Enum.at(map["assets"], 0)["id"] == Enum.at(asset_bundle.assets, 0).id
      assert Enum.at(map["assets"], 0)["type"] == Enumerations.UnityComponentType.from_integer(Enum.at(asset_bundle.assets, 0).type)
      assert Enum.at(map["assets"], 0)["path"] == Enum.at(asset_bundle.assets, 0).path
      assert Enum.at(map["assets"], 0)["name"] == Enum.at(asset_bundle.assets, 0).name
    end
  end
end
