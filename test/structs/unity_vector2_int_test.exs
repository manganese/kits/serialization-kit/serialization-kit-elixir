defmodule Manganese.SerializationKitTest.Structs.UnityVector2IntTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityVector2Int

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityVector2Int.from_map/1`" do
    @tag unity_math: true, unity_vector2_int: true
    test "it deserializes a Unity vector2 int from a map" do
      map = %{
        "x" => 1,
        "y" => -7
      }
      vector2_int = Structs.UnityVector2Int.from_map map

      assert vector2_int.x == map["x"]
      assert vector2_int.y == map["y"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector2Int.from_tuple/1`" do
    @tag unity_math: true, unity_vector2_int: true
    test "it deserializes a Unity vector2 int from a tuple" do
      tuple = {
        1,
        -7
      }
      vector2_int = Structs.UnityVector2Int.from_tuple tuple

      assert vector2_int.x == elem(tuple, 0)
      assert vector2_int.y == elem(tuple, 1)
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityVector2Int.to_map/1`" do
    @tag unity_math: true, unity_vector2_int: true
    test "it serializes a Unity vector2 int to a map" do
      vector2_int = %Structs.UnityVector2Int{
        x: 1,
        y: -7
      }
      map = Structs.UnityVector2Int.to_map vector2_int

      assert map["x"] == vector2_int.x
      assert map["y"] == vector2_int.y
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector2Int.to_tuple/1`" do
    @tag unity_math: true, unity_vector2_int: true
    test "it serializes a Unity vector2 int to a tuple" do
      vector2_int = %Structs.UnityVector2Int{
        x: 1,
        y: -7
      }
      tuple = Structs.UnityVector2Int.to_tuple vector2_int

      assert elem(tuple, 0) == vector2_int.x
      assert elem(tuple, 1) == vector2_int.y
    end
  end
end
