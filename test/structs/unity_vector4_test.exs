defmodule Manganese.SerializationKitTest.Structs.UnityVector4Test do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityVector4

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityVector4.from_map/1`" do
    @tag unity_math: true, unity_vector4: true
    test "it deserializes a Unity vector4 from a map" do
      map = %{
        "x" => 1,
        "y" => -2,
        "z" => 4.2,
        "w" => 35
      }
      vector4 = Structs.UnityVector4.from_map map

      assert vector4.x == map["x"]
      assert vector4.y == map["y"]
      assert vector4.z == map["z"]
      assert vector4.w == map["w"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector4.from_tuple/1`" do
    @tag unity_math: true, unity_vector4: true
    test "it deserializes a Unity vector4 from a tuple" do
      tuple = {
        1,
        -2,
        4.2,
        35
      }
      vector4 = Structs.UnityVector4.from_tuple tuple

      assert vector4.x == elem(tuple, 0)
      assert vector4.y == elem(tuple, 1)
      assert vector4.z == elem(tuple, 2)
      assert vector4.w == elem(tuple, 3)
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityVector4.to_map/1`" do
    @tag unity_math: true, unity_vector4: true
    test "it serializes a Unity vector4 to a map" do
      vector4 = %Structs.UnityVector4{
        x: 1,
        y: -2,
        z: 4.2,
        w: 35
      }
      map = Structs.UnityVector4.to_map vector4

      assert map["x"] == vector4.x
      assert map["y"] == vector4.y
      assert map["z"] == vector4.z
      assert map["w"] == vector4.w
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector4.to_tuple/1`" do
    @tag unity_math: true, unity_vector4: true
    test "it serializes a Unity vector4 to a tuple" do
      vector4 = %Structs.UnityVector4{
        x: 1,
        y: -2,
        z: 4.2,
        w: 35
      }
      tuple = Structs.UnityVector4.to_tuple vector4

      assert elem(tuple, 0) == vector4.x
      assert elem(tuple, 1) == vector4.y
      assert elem(tuple, 2) == vector4.z
      assert elem(tuple, 3) == vector4.w
    end
  end
end
