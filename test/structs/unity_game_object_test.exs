defmodule Manganese.SerializationKitTest.Structs.UnityGameObjectTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityGameObject

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityGameObject.from_map/1`" do
    @tag unity_game_object: true
    test "it deserializes a Unity game object name from a map" do
      map = %{
        "name" => "Player",
        "components" => [],
        "children" => []
      }
      game_object = Structs.UnityGameObject.from_map map

      assert game_object.name == map["name"]
    end

    @tag unity_game_object: true
    test "it deserializes a Unity game object component from a map" do
      map = %{
        "name" => "Player",
        "components" => [
          %{
            "id" => 1337,
            "type" => 8
          }
        ],
        "children" => []
      }
      game_object = Structs.UnityGameObject.from_map map

      assert Enum.at(game_object.components, 0).id == Enum.at(map["components"], 0)["id"]
      assert Enum.at(game_object.components, 0).type == Enumerations.UnityComponentType.from_integer(Enum.at(map["components"], 0)["type"])
    end

    @tag unity_game_object: true
    test "it deserializes a Unity game object child from a map" do
      map = %{
        "name" => "Player",
        "components" => [],
        "children" => [
          %{
            "name" => "Sword",
            "components" => [],
            "children" => []
          }
        ]
      }
      game_object = Structs.UnityGameObject.from_map map

      assert Enum.at(game_object.children, 0).name == Enum.at(map["children"], 0)["name"]
      assert length(Enum.at(game_object.children, 0).components) == length(Enum.at(map["children"], 0)["components"])
      assert length(Enum.at(game_object.children, 0).children) == length(Enum.at(map["children"], 0)["children"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityGameObject.to_map/1`" do
    @tag unity_game_object: true
    test "it serializes a Unity game object name to a map" do
      game_object = %Structs.UnityGameObject{
        name: "Player",
        components: [],
        children: []
      }
      map = Structs.UnityGameObject.to_map game_object

      assert map["name"] == game_object.name
    end

    @tag unity_game_object: true
    test "it serializes a Unity game object component to a map" do
      game_object = %Structs.UnityGameObject{
        name: "Player",
        components: [
          %Structs.UnityComponent{
            id: 1337,
            type: 8
          }
        ],
        children: []
      }
      map = Structs.UnityGameObject.to_map game_object

      assert Enum.at(map["components"], 0)["id"] == Enum.at(game_object.components, 0).id
      assert Enum.at(map["components"], 0)["type"] == Enumerations.UnityComponentType.from_integer(Enum.at(game_object.components, 0).type)
    end

    @tag unity_game_object: true
    test "it serializes a Unity game object child to a map" do
      game_object = %Structs.UnityGameObject{
        name: "Player",
        components: [],
        children: [
          %Structs.UnityGameObject{
            name: "Weapon",
            components: [],
            children: []
          }
        ]
      }
      map = Structs.UnityGameObject.to_map game_object

      assert Enum.at(map["children"], 0)["name"] == Enum.at(game_object.children, 0).name
      assert length(Enum.at(map["children"], 0)["components"]) == length(Enum.at(game_object.children, 0).components)
      assert length(Enum.at(map["children"], 0)["children"]) == length(Enum.at(game_object.children, 0).children)
    end
  end
end
