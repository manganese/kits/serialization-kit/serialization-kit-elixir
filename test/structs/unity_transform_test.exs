defmodule Manganese.SerializationKitTest.Structs.UnityTransformTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityTransform

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityTransform.from_map/1`" do
    @tag unity_math: true, unity_transform: true
    test "it deserializes a Unity transform from a map" do
      map = %{
        "position" => %{
          "x" => 1,
          "y" => -2,
          "z" => 4.2
        },
        "rotation" => %{
          "x" => -1.1,
          "y" => 0,
          "z" => 1,
          "w" => -0.5
        }
      }
      transform = Structs.UnityTransform.from_map map

      assert transform.position.x == map["position"]["x"]
      assert transform.position.y == map["position"]["y"]
      assert transform.position.z == map["position"]["z"]
      assert transform.rotation.x == map["rotation"]["x"]
      assert transform.rotation.y == map["rotation"]["y"]
      assert transform.rotation.z == map["rotation"]["z"]
      assert transform.rotation.w == map["rotation"]["w"]
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityTransform.to_map/1`" do
    @tag unity_math: true, unity_transform: true
    test "it serializes a Unity transform to a map" do
      transform = %Structs.UnityTransform{
        position: %Structs.UnityVector3{
          x: 1,
          y: -2,
          z: 4.2
        },
        rotation: %Structs.UnityQuaternion{
          x: -1.1,
          y: 0,
          z: 1,
          w: -0.5
        }
      }
      map = Structs.UnityTransform.to_map transform

      assert map["position"]["x"] == transform.position.x
      assert map["position"]["y"] == transform.position.y
      assert map["position"]["z"] == transform.position.z
      assert map["rotation"]["x"] == transform.rotation.x
      assert map["rotation"]["y"] == transform.rotation.y
      assert map["rotation"]["z"] == transform.rotation.z
      assert map["rotation"]["w"] == transform.rotation.w
    end
  end
end
