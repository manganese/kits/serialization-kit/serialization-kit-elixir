defmodule Manganese.SerializationKitTest.Structs.UnityVector3Test do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityVector3

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityVector3.from_map/1`" do
    @tag unity_math: true, unity_vector3: true
    test "it deserializes a Unity vector3 from a map" do
      map = %{
        "x" => 1,
        "y" => -2,
        "z" => 4.2
      }
      vector3 = Structs.UnityVector3.from_map map

      assert vector3.x == map["x"]
      assert vector3.y == map["y"]
      assert vector3.z == map["z"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector3.from_tuple/1`" do
    @tag unity_math: true, unity_vector3: true
    test "it deserializes a Unity vector3 from a tuple" do
      tuple = {
        1,
        -2,
        4.2
      }
      vector3 = Structs.UnityVector3.from_tuple tuple

      assert vector3.x == elem(tuple, 0)
      assert vector3.y == elem(tuple, 1)
      assert vector3.z == elem(tuple, 2)
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityVector3.to_map/1`" do
    @tag unity_math: true, unity_vector3: true
    test "it serializes a Unity vector3 to a map" do
      vector3 = %Structs.UnityVector3{
        x: 1,
        y: -2,
        z: 4.2
      }
      map = Structs.UnityVector3.to_map vector3

      assert map["x"] == vector3.x
      assert map["y"] == vector3.y
      assert map["z"] == vector3.z
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector3.to_tuple/1`" do
    @tag unity_math: true, unity_vector3: true
    test "it serializes a Unity vector3 to a tuple" do
      vector3 = %Structs.UnityVector3{
        x: 1,
        y: -2,
        z: 4.2
      }
      tuple = Structs.UnityVector3.to_tuple vector3

      assert elem(tuple, 0) == vector3.x
      assert elem(tuple, 1) == vector3.y
      assert elem(tuple, 2) == vector3.z
    end
  end
end
