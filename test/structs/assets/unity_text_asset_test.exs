defmodule Manganese.SerializationKitTest.Structs.UnityTextAssetTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityTextAsset

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityTextAsset.from_map/1`" do
    @tag unity_asset: true, unity_text_asset: true
    test "it deserializes a Unity text asset from a map" do
      map = %{
        "path" => "assets/text.txt",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:text)
      }
      text_asset = Structs.UnityTextAsset.from_map map

      assert text_asset.path == map["path"]
      assert text_asset.id == map["id"]
      assert text_asset.name == map["name"]
      assert text_asset.type == Enumerations.UnityAssetType.from_integer(map["type"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityTextAsset.to_map/1`" do
    @tag unity_asset: true, unity_text_asset: true
    test "it serializes a Unity text asset to a map" do
      text = %Structs.UnityTextAsset{
        path: "assets/text.txt",
        id: 1337,
        name: "Dirt"
      }
      map = Structs.UnityTextAsset.to_map text

      assert map["path"] == text.path
      assert map["id"] == text.id
      assert map["name"] == text.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(text.type)
    end
  end
end
