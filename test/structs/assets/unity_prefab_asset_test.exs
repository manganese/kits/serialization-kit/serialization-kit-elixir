defmodule Manganese.SerializationKitTest.Structs.UnityPrefabAssetTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityPrefabAsset

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityPrefabAsset.from_map/1`" do
    @tag unity_asset: true, unity_prefab_asset: true
    test "it deserializes a Unity prefab asset from a map" do
      map = %{
        "path" => "assets/prefab.mat",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:prefab),
        "root_game_object" => %{
          "name" => "Dirt",
          "components" => [],
          "children" => []
        }
      }
      prefab = Structs.UnityPrefabAsset.from_map map

      assert prefab.path == map["path"]
      assert prefab.id == map["id"]
      assert prefab.name == map["name"]
      assert prefab.type == Enumerations.UnityAssetType.from_integer(map["type"])
    end

    @tag unity_asset: true, unity_prefab_asset: true, unity_game_object: true
    test "it deserializes a Unity prefab asset root game object from a map" do
      map = %{
        "path" => "assets/prefab.mat",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:prefab),
        "root_game_object" => %{
          "name" => "Dirt",
          "components" => [],
          "children" => []
        }
      }
      prefab = Structs.UnityPrefabAsset.from_map map

      assert prefab.root_game_object.name == map["root_game_object"]["name"]
      assert length(prefab.root_game_object.components) == length(map["root_game_object"]["components"])
      assert length(prefab.root_game_object.children) == length(map["root_game_object"]["children"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityPrefabAsset.to_map/1`" do
    @tag unity_asset: true, unity_prefab_asset: true
    test "it serializes a Unity prefab asset to a map" do
      prefab = %Structs.UnityPrefabAsset{
        path: "assets/prefab.mat",
        id: 1337,
        name: "Dirt",
        root_game_object: %Structs.UnityGameObject{
          name: "Dirt",
          components: [],
          children: []
        }
      }
      map = Structs.UnityPrefabAsset.to_map prefab

      assert map["path"] == prefab.path
      assert map["id"] == prefab.id
      assert map["name"] == prefab.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(prefab.type)
    end

    @tag unity_asset: true, unity_prefab_asset: true, unity_game_object: true
    test "it serializes a Unity prefab asset root game object to a map" do
      prefab = %Structs.UnityPrefabAsset{
        path: "assets/prefab.mat",
        id: 1337,
        name: "Dirt",
        root_game_object: %Structs.UnityGameObject{
          name: "Dirt",
          components: [],
          children: []
        }
      }
      map = Structs.UnityPrefabAsset.to_map prefab

      assert map["root_game_object"]["name"] == prefab.root_game_object.name
      assert length(map["root_game_object"]["components"]) == length(prefab.root_game_object.components)
      assert length(map["root_game_object"]["children"]) == length(prefab.root_game_object.children)
    end
  end
end
