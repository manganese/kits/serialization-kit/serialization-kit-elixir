defmodule Manganese.SerializationKitTest.Structs.UnityAssetTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityAsset

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityAsset.from_map/1`" do
    @tag unity_asset: true
    test "it deserializes a Unity asset of an unknown type with the generic deserializer" do
      map = %{
        "path" => "assets/unknown",
        "id" => 1337,
        "name" => "Unknown",
        "type" => Enumerations.UnityAssetType.to_integer(:unknown)
      }
      asset = Structs.UnityAsset.from_map map
      
      assert %Structs.UnityAsset{} = asset
    end

    @tag unity_asset: true, unity_text_asset: true
    test "it deserializes a Unity text asset from a map with the generic deserializer" do
      map = %{
        "path" => "assets/text.txt",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:text)
      }
      text_asset = Structs.UnityAsset.from_map map

      assert %Structs.UnityTextAsset{} = text_asset
    end

    @tag unity_asset: true, unity_prefab_asset: true
    test "it deserializes a Unity prefab asset from a map with the generic deserializer" do
      map = %{
        "path" => "assets/prefab.mat",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:prefab),
        "root_game_object" => %{
          "name" => "Dirt",
          "components" => [],
          "children" => []
        }
      }
      prefab = Structs.UnityAsset.from_map map

      assert %Structs.UnityPrefabAsset{} = prefab
    end

    @tag unity_asset: true, unity_material_asset: true
    test "it deserializes a Unity material asset from a map with the generic deserializer" do
      map = %{
        "path" => "assets/material.mat",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:material)
      }
      material = Structs.UnityAsset.from_map map

      assert %Structs.UnityMaterialAsset{} = material
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityAsset.to_map/1`" do
    @tag unity_asset: true
    test "it serializes a Unity asset of an unknown type with the generic serializer" do
      asset = %Structs.UnityAsset{
        path: "assets/unknown",
        id: 1337,
        name: "Unknown"
      }
      map = Structs.UnityAsset.to_map asset
      
      assert map["path"] == asset.path
      assert map["id"] == asset.id
      assert map["name"] == asset.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(asset.type)
    end

    @tag unity_asset: true, unity_text_asset: true
    test "it serializes a Unity text asset to a map with the generic serializer" do
      text = %Structs.UnityTextAsset{
        path: "assets/text.txt",
        id: 1337,
        name: "Dirt"
      }
      map = Structs.UnityAsset.to_map text

      assert map["path"] == text.path
      assert map["id"] == text.id
      assert map["name"] == text.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(text.type)
    end

    @tag unity_asset: true, unity_prefab_asset: true
    test "it serializes a Unity prefab asset to a map with the generic serializer" do
      prefab = %Structs.UnityPrefabAsset{
        path: "assets/prefab.mat",
        id: 1337,
        name: "Dirt",
        root_game_object: %Structs.UnityGameObject{
          name: "Dirt",
          components: [],
          children: []
        }
      }
      map = Structs.UnityAsset.to_map prefab

      assert map["path"] == prefab.path
      assert map["id"] == prefab.id
      assert map["name"] == prefab.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(prefab.type)
    end

    @tag unity_asset: true, unity_material_asset: true
    test "it serializes a Unity material asset to a map with the generic serializer" do
      material = %Structs.UnityMaterialAsset{
        path: "assets/material.mat",
        id: 1337,
        name: "Dirt"
      }
      map = Structs.UnityAsset.to_map material

      assert map["path"] == material.path
      assert map["id"] == material.id
      assert map["name"] == material.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(material.type)
    end
  end
end
