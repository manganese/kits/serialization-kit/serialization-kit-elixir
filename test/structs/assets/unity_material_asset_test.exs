defmodule Manganese.SerializationKitTest.Structs.UnityMaterialAssetTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  doctest Manganese.SerializationKit.Structs.UnityMaterialAsset

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityMaterialAsset.from_map/1`" do
    @tag unity_asset: true, unity_material_asset: true
    test "it deserializes a Unity material asset from a map" do
      map = %{
        "path" => "assets/material.mat",
        "id" => 1337,
        "name" => "Dirt",
        "type" => Enumerations.UnityAssetType.to_integer(:material)
      }
      material = Structs.UnityMaterialAsset.from_map map

      assert material.path == map["path"]
      assert material.id == map["id"]
      assert material.name == map["name"]
      assert material.type == Enumerations.UnityAssetType.from_integer(map["type"])
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityMaterialAsset.to_map/1`" do
    @tag unity_asset: true, unity_material_asset: true
    test "it serializes a Unity material asset to a map" do
      material = %Structs.UnityMaterialAsset{
        path: "assets/material.mat",
        id: 1337,
        name: "Dirt"
      }
      map = Structs.UnityMaterialAsset.to_map material

      assert map["path"] == material.path
      assert map["id"] == material.id
      assert map["name"] == material.name
      assert map["type"] == Enumerations.UnityAssetType.to_integer(material.type)
    end
  end
end
