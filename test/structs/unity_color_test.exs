defmodule Manganese.SerializationKitTest.Structs.UnityColorTest do
  use ExUnit.Case

  alias Manganese.CoreKit

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityColor

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityColor.from_map/1`" do
    @tag unity_math: true, unity_color: true
    test "it deserializes a Unity color from a map" do
      map = %{
        "red" => 1,
        "green" => 0.25,
        "blue" => 0.5,
        "alpha" => 0.875
      }
      color = Structs.UnityColor.from_map map

      assert color.red == map["red"]
      assert color.green == map["green"]
      assert color.blue == map["blue"]
      assert color.alpha == map["alpha"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityColor.from_string/1`" do
    @tag unity_math: true, unity_color: true
    test "it deserializes a Unity color from a string" do
      string = "#C08000"
      { :ok, color } = Structs.UnityColor.from_string string

      assert color.red == 0.75
      assert color.green == 0.5
      assert color.blue == 0
      assert color.alpha == 1
    end

    @tag unity_math: true, unity_color: true
    test "it returns an API error when the string is not in the correct format" do
      string = "not valid"
      { :error, api_error } = Structs.UnityColor.from_string string

      assert api_error == %CoreKit.Structs.APIError{
        type: :unity_color_format_failure,
        status: :bad_request
      }
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityColor.to_map/1`" do
    @tag unity_math: true, unity_color: true
    test "it serializes a Unity color to a map" do
      color = %Structs.UnityColor{
        red: 0.75,
        green: 0.5,
        blue: 0.25,
        alpha: 0
      }
      map = Structs.UnityColor.to_map color

      assert map["red"] == color.red
      assert map["green"] == color.green
      assert map["blue"] == color.blue
      assert map["alpha"] == color.alpha
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityColor.to_string/1`" do
    @tag unity_math: true, unity_color: true
    test "it serializes a Unity color to a string" do
      color = %Structs.UnityColor{
        red: 0.75,
        green: 0.5,
        blue: 0
      }
      string = to_string color

      assert string == "#C08000"
    end
  end
end
