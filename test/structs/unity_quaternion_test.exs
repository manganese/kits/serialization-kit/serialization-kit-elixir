defmodule Manganese.SerializationKitTest.Structs.UnityQuaternionTest do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityQuaternion

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityQuaternion.from_map/1`" do
    @tag unity_math: true, unity_quaternion: true
    test "it deserializes a Unity quaternion from a map" do
      map = %{
        "x" => -2,
        "y" => 1.2,
        "z" => 0,
        "w" => 1
      }
      quaternion = Structs.UnityQuaternion.from_map map

      assert quaternion.w == map["w"]
      assert quaternion.x == map["x"]
      assert quaternion.y == map["y"]
      assert quaternion.z == map["z"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityQuaternion.from_tuple/1`" do
    @tag unity_math: true, unity_quaternion: true
    test "it deserializes a Unity quaternion from a tuple" do
      tuple = {
        -2,
        1.2,
        0,
        1
      }
      quaternion = Structs.UnityQuaternion.from_tuple tuple

      assert quaternion.x == elem(tuple, 0)
      assert quaternion.y == elem(tuple, 1)
      assert quaternion.z == elem(tuple, 2)
      assert quaternion.w == elem(tuple, 3)
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityQuaternion.to_map/1`" do
    @tag unity_math: true, unity_quaternion: true
    test "it serializes a Unity quaternion to a map" do
      quaternion = %Structs.UnityQuaternion{
        x: -2,
        y: 1.2,
        z: 0,
        w: 1
      }
      map = Structs.UnityQuaternion.to_map quaternion

      assert map["x"] == quaternion.x
      assert map["y"] == quaternion.y
      assert map["z"] == quaternion.z
      assert map["w"] == quaternion.w
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityQuaternion.to_tuple/1`" do
    @tag unity_math: true, unity_quaternion: true
    test "it serializes a Unity quaternion to a tuple" do
      quaternion = %Structs.UnityQuaternion{
        x: -2,
        y: 1.2,
        z: 0,
        w: 1
      }
      tuple = Structs.UnityQuaternion.to_tuple quaternion

      assert elem(tuple, 0) == quaternion.x
      assert elem(tuple, 1) == quaternion.y
      assert elem(tuple, 2) == quaternion.z
      assert elem(tuple, 3) == quaternion.w
    end
  end
end
