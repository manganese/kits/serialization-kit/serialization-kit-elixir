defmodule Manganese.SerializationKitTest.Structs.UnityVector2Test do
  use ExUnit.Case

  alias Manganese.SerializationKit.Structs

  doctest Manganese.SerializationKit.Structs.UnityVector2

  # Deserialization
  describe "`Manganese.SerializationKit.Structs.UnityVector2.from_map/1`" do
    @tag unity_math: true, unity_vector2: true
    test "it deserializes a Unity vector2 from a map" do
      map = %{
        "x" => 1,
        "y" => -4.2
      }
      vector2 = Structs.UnityVector2.from_map map

      assert vector2.x == map["x"]
      assert vector2.y == map["y"]
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector2.from_tuple/1`" do
    @tag unity_math: true, unity_vector2: true
    test "it deserializes a Unity vector2 from a tuple" do
      tuple = {
        1,
        -4.2
      }
      vector2 = Structs.UnityVector2.from_tuple tuple

      assert vector2.x == elem(tuple, 0)
      assert vector2.y == elem(tuple, 1)
    end
  end

  # Serialization
  describe "`Manganese.SerializationKit.Structs.UnityVector2.to_map/1`" do
    @tag unity_math: true, unity_vector2: true
    test "it serializes a Unity vector2 to a map" do
      vector2 = %Structs.UnityVector2{
        x: 1,
        y: -4.2
      }
      map = Structs.UnityVector2.to_map vector2

      assert map["x"] == vector2.x
      assert map["y"] == vector2.y
    end
  end

  describe "`Manganese.SerializationKit.Structs.UnityVector2.to_tuple/1`" do
    @tag unity_math: true, unity_vector2: true
    test "it serializes a Unity vector2 to a tuple" do
      vector2 = %Structs.UnityVector2{
        x: 1,
        y: -4.2
      }
      tuple = Structs.UnityVector2.to_tuple vector2

      assert elem(tuple, 0) == vector2.x
      assert elem(tuple, 1) == vector2.y
    end
  end
end
