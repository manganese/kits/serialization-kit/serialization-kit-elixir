# Manganese SerializationKit - Elixir

*Extension for de/serializing Unity data structures from SerializationKit in Elixir*

SerializationKit for Unity enables you to export Unity data structures such as assets, game objects, and components to web services.  This library provides structs and enumerations for representing your Unity data in Elixir and Ecto.

## Installation

```elixir
def deps do
  [
    { :manganese_serialization_kit, "~> 0.2" }
  ]
end
```

## Documentation

To generate the documentation from the source:

```bash
mix docs
```

## Testing

To run the automated tests:

```bash
mix test
```

## Basic usage

### Deserialization

Use the deserialization functions to import data from SerializationKit in Unity:

```elixir
# Deserialize asset bundle
unity_asset_bundle = Manganese.SerializationKit.Structs.UnityAssetBundle.from_map %{
  assets: [
    %{
      path:  "assets/materials/test-material.mat",
      id: 86791,
      name: "Test Material",
      type: Manganese.SerializationKit.Enumerations.UnityAssetType.to_integer(:material)
    }
  ]
}
```

Similar functions are available for most structs in the library.

### Serialization

To convert a SerializationKit struct back to a map (perhaps before serializing it as JSON and sending it to another client), use the corresponding serialization functions:

```elixir
asset_bundle_map = Manganese.SerializationKit.Structs.UnityAssetBundle.to_map %Manganese.SerializationKit.Structs.UnityAssetBundle{
  assets: [
    %Manganese.SerializationKit.Structs.UnityMaterialAsset{
      path:  "assets/materials/test-material.mat",
      id: 86791,
      name: "Test Material"
    }
  ]
}
```

Similar functions are available for most structs in the library.

## Use with Ecto

Some structs can also be used as Ecto types, such as `UnityVector3`, `UnityQuaternion`, and `UnityAssetBundle`.  Just add the type's module to your schema like any other custom type:

```elixir
  schema "assets" do
    field :asset_bundle, Manganese.SerializationKit.Structs.UnityAssetBundle

    # ...
  end
```