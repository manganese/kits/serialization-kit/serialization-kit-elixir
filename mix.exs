defmodule Manganese.SerializationKit.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese_serialization_kit,
      version: "0.2.11",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # ExDoc
      { :ex_doc, "~> 0.19", only: [ :dev, :gitlab ], runtime: false },

      # Ecto
      { :ecto, "~> 3.6" },

      # Manganese CoreKit
      { :manganese_core_kit, "~> 0.1" }
    ]
  end

  defp package do
    [
      name: "manganese_serialization_kit",
      description: "Library for using Unity data structures from SerializationKit in Elixir",
      licenses: [ "Apache 2.0" ],
      links: %{
        "GitLab" => "https://gitlab.com/manganese/kits/serialization-kit/serialization-kit-elixir"
      },
      maintainers: [
        "Samuel Goodell"
      ]
    ]
  end
end
