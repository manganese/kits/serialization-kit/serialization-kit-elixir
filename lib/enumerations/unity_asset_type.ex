defmodule Manganese.SerializationKit.Enumerations.UnityAssetType do
  @moduledoc """

  A type of Unity asset that can be included in an asset bundle.

  *See `Manganese.SerializationKit.Structs.UnityAssetBundle`*
  *See `Manganese.SerializationKit.Structs.UnityAsset`*

  """

  @typedoc """

  An internal representation of a Unity asset type.

  """
  @type t ::
    :unknown |
    :text |
    :prefab |
    :material

  # Deserialization

  @doc """

  """
  @spec from_integer(t_external) :: t
  def from_integer(integer) do
    %{
      0 => :unknown,
      1 => :text,
      2 => :prefab,
      3 => :material
    }[integer]
  end


  # Serialization

  @typedoc """

  An external representation of a Unity asset type.

  """
  @type t_external :: non_neg_integer

  @doc """

  """
  @spec to_integer(t) :: t_external
  def to_integer(value) do
    %{
      unknown:  0,
      text:     1,
      prefab:   2,
      material: 3
    }[value]
  end
end