defmodule Manganese.SerializationKit.Enumerations.UnityComponentType do
  @moduledoc """

  A type of Unity component that can be associated with a Unity game object.

  *See `Manganese.SerializationKit.Structs.UnityGameObject`*

  """

  @typedoc """

  An internal representation of a Unity component type.

  """
  @type t ::
    :unknown |
    :transform |
    :box_collider

  # Deserialization

  @doc """

  """
  @spec from_integer(t_external) :: t
  def from_integer(integer) do
    %{
      0 => :unknown,
      1 => :transform,
      2 => :box_collider
    }[integer]
  end


  # Serialization

  @typedoc """

  An external representation of a Unity component type.

  """
  @type t_external :: non_neg_integer

  @doc """

  """
  @spec to_integer(t) :: t_external
  def to_integer(value) do
    %{
      unknown:      0,
      transform:    1,
      box_collider: 2
    }[value]
  end
end