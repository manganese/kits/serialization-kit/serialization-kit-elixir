defmodule Manganese.SerializationKit.Migrations.CreateUnityVector3Migration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration

      def up do
        execute """
          CREATE TYPE unity_vector3 AS (
            x float,
            y float,
            z float
          );
        """
      end

      def down do
        execute """
          DROP TYPE unity_vector3;
        """
      end
    end
  end
end