defmodule Manganese.SerializationKit.Migrations.CreateUnityVector2IntMigration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration

      def up do
        execute """
          CREATE TYPE unity_vector2_int AS (
            x integer,
            y integer
          );
        """
      end

      def down do
        execute """
          DROP TYPE unity_vector2_int;
        """
      end
    end
  end
end
