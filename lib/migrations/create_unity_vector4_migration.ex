defmodule Manganese.SerializationKit.Migrations.CreateUnityVector4Migration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration

      def up do
        execute """
          CREATE TYPE unity_vector4 AS (
            x float,
            y float,
            z float,
            w float
          );
        """
      end

      def down do
        execute """
          DROP TYPE unity_vector4;
        """
      end
    end
  end
end