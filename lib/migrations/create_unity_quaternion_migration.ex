defmodule Manganese.SerializationKit.Migrations.CreateUnityQuaternionMigration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration

      def up do
        execute """
          CREATE TYPE unity_quaternion AS (
            x float,
            y float,
            z float,
            w float
          );
        """
      end

      def down do
        execute """
          DROP TYPE unity_quaternion;
        """
      end
    end
  end
end