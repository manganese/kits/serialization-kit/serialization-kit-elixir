defmodule Manganese.SerializationKit.Migrations.CreateUnityVector2Migration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration

      def up do
        execute """
          CREATE TYPE unity_vector2 AS (
            x float,
            y float
          );
        """
      end

      def down do
        execute """
          DROP TYPE unity_vector2;
        """
      end
    end
  end
end
