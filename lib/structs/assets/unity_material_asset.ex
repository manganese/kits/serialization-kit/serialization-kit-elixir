defmodule Manganese.SerializationKit.Structs.UnityMaterialAsset do
  @moduledoc """

  A Unity material asset.

  ## Deserialization

  *See `from_map/1`*

  ## Serialization

  *See `to_map/1`*

  """

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  @typedoc """

  A Unity material asset.

  """
  @type t :: %Structs.UnityMaterialAsset{
    path: String.t,
    id: Structs.UnityAsset.id,
    name: String.t,
    type: Enumerations.UnityAssetType.t
  }
  @enforce_keys [
    :path,
    :id,
    :name
  ]
  defstruct [
    :path,
    :id,
    :name,
    type: :material
  ]

  # Deserialization

  @doc """

  Deserialize a material asset from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{
    "path" => path,
    "id" => id,
    "name" => name
  }) do
    %Structs.UnityMaterialAsset{
      id: id,
      name: name,
      path: path
    }
  end


  # Serialization
  @type t_external :: map

  @doc """

  Serialize a material asset to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.UnityMaterialAsset{
    path: path,
    id: id,
    name: name,
    type: type
  }) do
    %{
      "path" => path,
      "id" => id,
      "name" => name,
      "type" => Enumerations.UnityAssetType.to_integer(type)
    }
  end
end