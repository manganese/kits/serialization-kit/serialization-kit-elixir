defmodule Manganese.SerializationKit.Structs.UnityPrefabAsset do
  @moduledoc """

  A Unity prefab asset.

  ## Deserialization

  *See `from_map/1`*

  ## Serialization

  *See `to_map/1`*

  """

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  @typedoc """

  A Unity prefab asset.

  """
  @type t :: %Structs.UnityPrefabAsset{
    path: String.t,
    id: Structs.UnityAsset.id,
    name: String.t,
    root_game_object: String.t,
    type: Enumerations.UnityAssetType.t
  }
  @enforce_keys [
    :path,
    :id,
    :name,
    :root_game_object
  ]
  defstruct [
    :path,
    :id,
    :name,
    :root_game_object,
    type: :prefab
  ]

  # Deserialization

  @doc """

  Deserialize a prefab asset from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{ "path" => path, "id" => id, "name" => name, "root_game_object" => root_game_object }) do
    %Structs.UnityPrefabAsset{
      path: path,
      id: id,
      name: name,
      root_game_object: Structs.UnityGameObject.from_map root_game_object
    }
  end


  # Serialization
  @type t_external :: map

  @doc """

  Serialize a prefab asset to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.UnityPrefabAsset{
    path: path,
    id: id,
    name: name,
    root_game_object: root_game_object,
    type: type
  }) do
    %{
      "path" => path,
      "id" => id,
      "name" => name,
      "root_game_object" => Structs.UnityGameObject.to_map(root_game_object),
      "type" => Enumerations.UnityAssetType.to_integer(type)
    }
  end
end