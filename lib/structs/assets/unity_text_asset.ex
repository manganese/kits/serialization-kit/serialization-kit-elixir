defmodule Manganese.SerializationKit.Structs.UnityTextAsset do
  @moduledoc """

  A Unity text asset.

  ## Deserialization

  *See `from_map/1`*

  ## Serialization

  *See `to_map/1`*

  """

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  @typedoc """

  A Unity text asset.

  """
  @type t :: %Structs.UnityTextAsset{
    path: String.t,
    id: Structs.UnityAsset.id,
    name: String.t,
    type: Enumerations.UnityAssetType.t
  }
  @enforce_keys [
    :path,
    :id,
    :name
  ]
  defstruct [
    :path,
    :id,
    :name,
    type: :text
  ]

  # Deserialization

  @doc """

  Deserialize a text asset from a map.

  """
  @spec from_map(map) :: t
  def from_map(%{
    "path" => path,
    "id" => id,
    "name" => name
  }) do
    %Structs.UnityTextAsset{
      path: path,
      id: id,
      name: name
    }
  end


  # Serialization

  @doc """

  Serialize a text asset to a map.

  """
  @spec to_map(t) :: map
  def to_map(%Structs.UnityTextAsset{
    path: path,
    id: id,
    name: name,
    type: type
  }) do
    %{
      "path" => path,
      "id" => id,
      "name" => name,
      "type" => Enumerations.UnityAssetType.to_integer(type)
    }
  end
end