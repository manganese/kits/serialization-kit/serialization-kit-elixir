defmodule Manganese.SerializationKit.Structs.UnityBoxColliderComponent do
  @moduledoc """

  A Unity box collider component.

  ## Deserialization

  *See `from_map/1`*

  ## Serialization

  *See `to_map/1`*

  """

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  @typedoc """

  A Unity box collider component.

  """
  @type t :: %Structs.UnityBoxColliderComponent{
    id: Structs.UnityComponent.id,
    type: Enumerations.UnityComponentType.t
  }
  @enforce_keys [
    :id
  ]
  defstruct [
    :id,
    type: :box_collider
  ]

  # Deserialization

  @doc """

  Deserialize a box collider component from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{ "id" => id }) do
    %Structs.UnityBoxColliderComponent{
      id: id
    }
  end


  # Serialization
  @type t_external :: map

  @doc """

  Serialize a box collider component to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.UnityBoxColliderComponent{
    id: id,
    type: type
  }) do
    %{
      "id" => id,
      "type" => Enumerations.UnityComponentType.to_integer(type)
    }
  end
end