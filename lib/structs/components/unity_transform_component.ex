defmodule Manganese.SerializationKit.Structs.UnityTransformComponent do
  @moduledoc """

  A Unity transform component with a position, rotation, and scale.

  ## Deserialization

  *See `from_map/1`*

  ## Serialization

  *See `to_map/1`*

  """

  alias Manganese.SerializationKit.Structs
  alias Manganese.SerializationKit.Enumerations

  @typedoc """

  A Unity transform component.

  """
  @type t :: %Structs.UnityTransformComponent{
    id: Structs.UnityComponent.id,
    type: Enumerations.UnityComponentType.t
  }
  @enforce_keys [
    :id
  ]
  defstruct [
    :id,
    type: :transform
  ]

  # Deserialization

  @doc """

  Deserialize a transform component from a map.

  """
  @spec from_map(map) :: t
  def from_map(%{ "id" => id }) do
    %Structs.UnityTransformComponent{
      id: id
    }
  end


  # Serialization

  @doc """

  Serialize a transform component to a map.

  """
  @spec to_map(t) :: map
  def to_map(%Structs.UnityTransformComponent{
    id: id,
    type: type
  }) do
    %{
      "id" => id,
      "type" => Enumerations.UnityComponentType.to_integer(type)
    }
  end
end