defmodule Manganese.SerializationKit.Structs.UnityColor do
  @moduledoc """

  A Unity color stored in RGB/RGBA format.

  This module can be used as an Ecto type for de/serialization when interacting with the database.

  ## Deserialization

  *See `from_string/1`*

  ## Serialization

  The color struct implements the `String.Chars` protocol.

  """

  alias Manganese.CoreKit

  alias Manganese.SerializationKit.Structs

  @typedoc """

  A Unity color.

  """
  @type t :: %Structs.UnityColor{
    red: float,
    green: float,
    blue: float,
    alpha: float
  }
  @enforce_keys [
    :red,
    :green,
    :blue
  ]
  defstruct [
    :red,
    :green,
    :blue,
    alpha: 1
  ]


  # Deserialization

  @spec parse_components([ String.t ]) :: [ float ]
  defp parse_components(component_strings) do
    component_strings
    |> Enum.map(&parse_component/1)
  end

  @spec parse_component(String.t) :: float
  defp parse_component(component_string) do
    { component, _ } = Integer.parse(component_string, 16)

    component / 256
  end

  @doc """

  Deserialize a color from a string.

  The expected format is a "#" followed by 3, 4, 6, or 8 hexadecimal digits.

  """
  @spec from_string(String.t) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def from_string(string) do
    components_string =
      case string do
        "#" <> components_string -> components_string
        components_string -> components_string
      end

    case (
      cond do
        String.length(components_string) == 8 ->
          red = String.slice components_string, 0..1
          green = String.slice components_string, 2..3
          blue = String.slice components_string, 4..5
          alpha = String.slice components_string, 6..7

          [ red, green, blue, alpha ]
          |> parse_components
        String.length(components_string) == 6 ->
          red = String.slice components_string, 0..1
          green = String.slice components_string, 2..3
          blue = String.slice components_string, 4..5

          [ red, green, blue ]
          |> parse_components
        String.length(components_string) == 4 ->
          red = String.slice components_string, 0..0
          green = String.slice components_string, 1..1
          blue = String.slice components_string, 2..2
          alpha = String.slice components_string, 3..3

          [ red, green, blue, alpha ]
          |> parse_components
          |> Enum.map(fn component -> component * (16 + 1) end)
        String.length(components_string) === 3 ->
          red = String.slice components_string, 0..0
          green = String.slice components_string, 1..1
          blue = String.slice components_string, 2..2

          [ red, green, blue ]
          |> parse_components
          |> Enum.map(fn component -> component * (16 + 1) end)
        true ->
          { :error, %CoreKit.Structs.APIError{
            type: :unity_color_format_failure,
            status: :bad_request
          }}
      end
    ) do
      components when is_list components ->
        [ red, green, blue ] = components

        color = %Structs.UnityColor{
          red: red,
          green: green,
          blue: blue
        }

        color =
          if length(components) > 4 do
            color
            |> Map.put(:alpha, Enum.at(components, 3))
          else
            color
          end

        { :ok, color }
      { :error, api_error } -> { :error, api_error }
    end
  end

  @spec from_string!(String.t) :: t
  def from_string!(string) do
    { :ok, color } = from_string string

    color
  end

  @doc """

  Deserialize a color from a map.

  """
  @spec from_map(map) :: t
  def from_map(%{
    "red" => red,
    "green" => green,
    "blue" => blue
  } = map) do
    %Structs.UnityColor{
      red: red,
      green: green,
      blue: blue,
      alpha: Map.get(map, "alpha", 1)
    }
  end


  # Serialization

  @doc """

  Serialize a color to a map.

  """
  @spec to_map(t) :: map
  def to_map(%Structs.UnityColor{
    red: red,
    green: green,
    blue: blue,
    alpha: alpha
  }) do
    %{
      "red" => red,
      "green" => green,
      "blue" => blue,
      "alpha" => alpha
    }
  end


  # Ecto type
  use Ecto.Type

  @doc """

  The PostgreSQL composite type used to represent a color.

  """
  @spec type :: :string
  def type, do: :string

  @doc false
  def cast(%Structs.UnityColor{} = color), do: { :ok, color }
  def cast(string) when is_binary(string), do: from_string string
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  @doc false
  def load(string) when is_binary(string), do: from_string string

  @doc false
  def dump(%Structs.UnityColor{} = color), do: { :ok, to_string color }


  # Preset colors
  def white,   do: from_string! "#FFFFFF"
  def red,     do: from_string! "#FF0000"
  def orange,  do: from_string! "#FF8000"
  def yellow,  do: from_string! "#FFFF00"
  def green,   do: from_string! "#00FF00"
  def cyan,    do: from_string! "#00FFFF"
  def blue,    do: from_string! "#0000FF"
  def magenta, do: from_string! "#FF00FF"
  def black,   do: from_string! "#000000"
end


defimpl String.Chars, for: Manganese.SerializationKit.Structs.UnityColor do
  def to_string(color) do
    components_string =
      [
        :red,
        :green,
        :blue
      ]
      |> Enum.map(fn component_string -> Map.get(color, component_string) end)
      |> Enum.map(fn component -> Integer.to_string(trunc(component * 256), 16) end)
      |> Enum.map(fn string -> String.pad_leading(string, 2, "0") end)
      |> Enum.join()

    components_string =
      cond do
        color.alpha != 1 ->
          components_string <> Integer.to_charlist(trunc(color.alpha * 256), 16)
        true -> components_string
      end

    "#" <> components_string
  end
end