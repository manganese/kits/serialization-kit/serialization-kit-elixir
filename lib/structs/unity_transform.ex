defmodule Manganese.SerializationKit.Structs.UnityTransform do
  @moduledoc """

  """

  alias Manganese.SerializationKit.Structs

  @typedoc """

  A Unity transform.

  """
  @type t :: %Structs.UnityTransform{
    position: Structs.UnityVector3.t,
    rotation: Structs.UnityQuaternion.t
  }
  defstruct [
    :position,
    :rotation
  ]

  # Deserialization

  @doc """

  Deserialize a transform from a map.

  """
  @spec from_map(map) :: t
  def from_map(%{ "position" => position, "rotation" => rotation }) do
    %Structs.UnityTransform{
      position: Structs.UnityVector3.from_map(position),
      rotation: Structs.UnityQuaternion.from_map(rotation)
    }
  end


  # Serialization

  @doc """

  Serialize a transform to a map.

  """
  @spec to_map(t) :: map
  def to_map(%Structs.UnityTransform{
    position: position,
    rotation: rotation
  }) do
    %{
      "position" => Structs.UnityVector3.to_map(position),
      "rotation" => Structs.UnityQuaternion.to_map(rotation)
    }
  end
end