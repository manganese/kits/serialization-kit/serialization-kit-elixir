defmodule Manganese.SerializationKit.Structs.UnityGameObject do
  @moduledoc """

  A Unity game object.

  ## Deserialization

  *See `from_map`*

  ## Serialization

  *See `to_map`*

  """

  alias Manganese.SerializationKit.Structs

  @typedoc """

  A Unity game object.

  """
  @type t :: %Structs.UnityGameObject{
    name: String.t,
    components: [ Structs.UnityComponent.t  ],
    children: [ Structs.UnityGameObject.t ]
  }
  @enforce_keys [
    :name,
    :components,
    :children
  ]
  defstruct [
    :name,
    :components,
    :children
  ]

  # Deserialization

  @doc """

  Deserialize a game object from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{
    "name" => name,
    "components" => components,
    "children" => children
  }) do
    %Structs.UnityGameObject{
      name: name,
      components: components |> Enum.map(&Structs.UnityComponent.from_map/1),
      children: children |> Enum.map(&from_map/1)
    }
  end


  # Serialization
  @type t_external :: map

  @doc """

  Serialize a game object to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.UnityGameObject{
    name: name,
    components: components,
    children: children
  }) do
    %{
      "name" => name,
      "components" => components |> Enum.map(&Structs.UnityComponent.to_map/1),
      "children" => children |> Enum.map(&to_map/1)
    }
  end
end