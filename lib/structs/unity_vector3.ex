defmodule Manganese.SerializationKit.Structs.UnityVector3 do
  @moduledoc """

  A Unity vector3 value with `:x`, `:y`, and `:z` components.

  *Note that Unity structs are available in order to process the data in the API server, but most vector calculations occur in the environment simulation service.*

  This module can be used as an Ecto type for de/serialization when interacting with the database.

  """

  alias Manganese.SerializationKit.Structs

  @typedoc """

  A Unity vector3.

  """
  @type t :: %Structs.UnityVector3{
    x: float,
    y: float,
    z: float
  }
  defstruct [
    :x,
    :y,
    :z
  ]

  # Helpers

  @doc """

  A vector3 with zero values for all components.

  """
  @spec zero :: t
  def zero, do: %Structs.UnityVector3{
    x: 0,
    y: 0,
    z: 0
  }

  # Deserialization

  @doc """

  Deserialize a vector3 from a map.

  """
  @spec from_map(t_external) :: t
  def from_map(%{
    "x" => x,
    "y" => y,
    "z" => z
  }) do
    %Structs.UnityVector3{
      x: x,
      y: y,
      z: z
    }
  end

  @doc """

  Deserialize a vector3 from a tuple.

  """
  @spec from_tuple(t_internal) :: t
  def from_tuple({ x, y, z }) do
    %Structs.UnityVector3{
      x: x,
      y: y,
      z: z
    }
  end


  # Serialization
  @type t_internal :: { float, float, float }
  @type t_external :: map

  @doc """

  Serialize a vector3 to a map.

  """
  @spec to_map(t) :: t_external
  def to_map(%Structs.UnityVector3{
    x: x,
    y: y,
    z: z
  }) do
    %{
      "x" => x,
      "y" => y,
      "z" => z
    }
  end

  @doc """

  Serialize a vector3 to a tuple.

  """
  @spec to_tuple(t) :: t_internal
  def to_tuple(%Structs.UnityVector3{
    x: x,
    y: y,
    z: z
  }) do
    { x, y, z }
  end

  # Ecto type
  use Ecto.Type

  @doc """

  The PostgreSQL composite type used to represent a vector3.

  """
  def type, do: :unity_vector3

  def cast(%Structs.UnityVector3{} = unity_vector3), do: { :ok, unity_vector3 }
  def cast({ _x, _y, _z } = tuple), do: { :ok, from_tuple tuple }
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  def load({ _x, _y, _z } = tuple), do: { :ok, from_tuple tuple }
  def load(nil), do: { :ok, nil }
  def load(_), do: :error

  def dump(%Structs.UnityVector3{} = unity_vector3), do: { :ok, to_tuple unity_vector3 }
  def dump(nil), do: { :ok, nil }
  def dump(_), do: :error
end